# Syeve – Simple yet efficient video encoding

This is a lossless video codec, aiming at screen capture and anything else you'd want to format in PNG instead of JPEG. In fact, it is based on [QOI](https://qoiformat.org/) which has advantages over PNG.

The difference between Syeve and just sending dumb QOI-encoded frames is that we filter each frame as the difference with the previous one. This way, streaming almost identical frames can be encoded very efficiently.

## Example

Run a P2P video-conferencing example:

    # Run these commands in two different terminals or machines
    # "client" streams screen capture to the given address and "server" listens to the given address to display the stream
    cargo run --release -p conference -- server 0.0.0.0:8080
    cargo run --release -p conference -- client 127.0.0.1:8080

(change `127.0.0.1` to the server address)

```rust
let mut input_raw_frame: Vec<u8> = my_rgb24_bitmap_of_size_1920x1080();

let mut buf = Vec::new();
let size = (1920, 1080);
let pixel_size = 3; // 3 for RGB, 4 for RGBA
let seq_size = 300; // send a full frame every `seq_size` frames
let mut encoder = syeve::Encoder::new(size, pixel_size, seq_size);
encoder.encode(&mut input_raw_frame, &mut buf).unwrap();

let mut decoder = syeve::Decoder::new();
let output_raw_frame = decoder.decode(&buf).unwrap().pixels;
```

## Motivation

Lossy video formats are very efficient, but:
* they are super complex and involve lots of mathematical concepts that you can't understand unless it's your research field;
* hence, they have a very few implementations;
* none of these implementations are easy to use for a programmer;
* they perform badly on screen captures, both in bandwidth and legibility;
* video processing is fun.

For example, almost every software using ffmpeg uses its CLI rather than ABI. This results in the need of writing files on the disk when unneeded (or even writing every frame of a video in JPEG or PNG) and a complicated handling of the process by the caller.

## Status

⚠ **unstable, not benchmarked, not fuzz-tested**... but promising 😎

* [ ] Optional lossy optimization?
  * [ ] posterization?
  * [ ] FFT encoding?
* Benchmark
  * [ ] Best sequence length
  * [ ] Micro-optimization
* [ ] Avoid allocations
* QOI
  * [ ] Allow 1-channel color
  * [ ] Remove redundant header

Single frame encoding is faster than with `image-rs/image-png` (thanks to SIMD optimization).

Any help with optimization (potentially lossy) or benchmark or else would be greatly appreciated. (PR/issue/email welcome)

Currently a fork of QOI is used, for optimization.

## Protocol

The protocol is extremely simple:

* sequence number (u32 BE)
* frame number (u32 BE)
* width (u32 BE)
* height (u32 BE)
* pixel size (u8)
* QOI payload size (u32 BE)
* QOI payload

If frame number is non-zero, then the (decoded) frame is the byte-per-byte wrapping difference with the previous frame.

The first frame should have frame number to zero. Frame number must be incremented by one between each frame. Because each frame depends on the previous frame, a frame loss may result in a broken frame for the decoder. This problem is reduced by sending periodically a fully self-described frame. This frame should have frame number to zero, and a sequence number different from the previous frame (typically incremented by one). The sequence number of the first frame can be random.

## Sources

* [QOI format](https://qoiformat.org/)

[Previously](https://framagit.org/ZettaScript/syeve/-/tree/f3b6640945f7c9fd6d6ac20b6063426a92884baa), an algo inspired by PNG was used, but the compression was too CPU-heavy, so QOI is preferred now. For the curious, here are the old sources:

* [Image file compression made easy, Paeth, 1991](https://sci-hub.se/downloads/2020-11-17/14/paeth1991.pdf)
* [PNG specification](https://www.w3.org/TR/PNG/#9Filters)
* [PNG, Wikipedia](https://en.wikipedia.org/wiki/Portable_Network_Graphics)
* [The case of the missing SIMD code, Larry Bank, 2021](https://optidash.ai/blog/the-case-of-the-missing-simd-code)
* [libpng SSE2 implementation](https://github.com/glennrp/libpng/blob/a37d4836519517bdce6cb9d956092321eca3e73b/intel/filter_sse2_intrinsics.c)

[Donate on LiberaPay](https://liberapay.com/tuxmain)

## License

CopyLeft 2021-2022 Pascal Engélibert

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, version 3 of the License.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see https://www.gnu.org/licenses/.
