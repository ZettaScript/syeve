#[cfg(feature = "simd")]
use packed_simd::u8x32;

pub fn encode(
	size: (usize, usize),
	pixel_size: usize,
	pixels: &mut [u8],
	old_pixels: &mut [u8],
	output: &mut [u8],
) -> usize {
	assert!(size.0 < u32::MAX as usize);
	assert!(size.1 < u32::MAX as usize);
	assert!(pixel_size < u8::MAX as usize);
	assert_eq!(pixels.len(), size.0 * size.1 * pixel_size);
	assert_eq!(pixels.len(), old_pixels.len());

	#[cfg(feature = "simd")]
	{
		let mut pixels_iter = pixels.chunks_exact_mut(32);
		let mut old_pixels_iter = old_pixels.chunks_exact_mut(32);
		while let (Some(x), Some(z)) = (pixels_iter.next(), old_pixels_iter.next()) {
			let zv = u8x32::from_slice_unaligned(z);
			z.copy_from_slice(x);
			(u8x32::from_slice_unaligned(x) - zv).write_to_slice_unaligned(x);
		}
		let rem_iter = pixels_iter
			.into_remainder()
			.iter_mut()
			.zip(old_pixels_iter.into_remainder().iter_mut());
		for (x, z) in rem_iter {
			let old_z = *z;
			*z = *x;
			*x = x.wrapping_sub(old_z);
		}
	}
	#[cfg(not(feature = "simd"))]
	for (x, z) in pixels.iter_mut().zip(old_pixels.iter_mut()) {
		let old_z = *z;
		*z = *x;
		*x = x.wrapping_sub(old_z);
	}

	qoi::encode_to_buf(output, pixels, size.0 as u32, size.1 as u32).unwrap()
}

#[allow(clippy::useless_asref)]
pub fn decode(
	size: (usize, usize),
	pixel_size: usize,
	pixels: &mut [u8],
	old_pixels: &mut [u8],
	data: &[u8],
) {
	assert!(size.0 < u32::MAX as usize);
	assert!(size.1 < u32::MAX as usize);
	assert!(pixel_size < u8::MAX as usize);
	assert_eq!(pixels.len(), size.0 * size.1 * pixel_size);
	assert_eq!(pixels.len(), old_pixels.len());

	let header = qoi::decode_to_buf(pixels.as_mut(), data).unwrap();
	assert_eq!(header.width, size.0 as u32);
	assert_eq!(header.height, size.1 as u32);
	assert_eq!(header.channels.as_u8(), pixel_size as u8);

	#[cfg(feature = "simd")]
	{
		let mut pixels_iter = pixels.chunks_exact_mut(32);
		let mut old_pixels_iter = old_pixels.chunks_exact_mut(32);
		while let (Some(x), Some(z)) = (pixels_iter.next(), old_pixels_iter.next()) {
			(u8x32::from_slice_unaligned(x) + u8x32::from_slice_unaligned(z))
				.write_to_slice_unaligned(x);
			z.copy_from_slice(x);
		}
		let rem_iter = pixels_iter
			.into_remainder()
			.iter_mut()
			.zip(old_pixels_iter.into_remainder().iter_mut());
		for (x, z) in rem_iter {
			*x = x.wrapping_add(*z);
			*z = *x;
		}
	}
	#[cfg(not(feature = "simd"))]
	for (x, z) in pixels.iter_mut().zip(old_pixels.iter_mut()) {
		*x = x.wrapping_add(*z);
		*z = *x;
	}
}

// OLD (for benchmarking spatial flattening)

fn paeth(a: u8, b: u8, c: u8) -> u8 {
	let (a, b, c) = (a as i16, b as i16, c as i16);

	let mut pa = b - c;
	let mut pb = a - c;
	let mut pc = pa + pb;

	pa = pa.abs();
	pb = pb.abs();
	pc = pc.abs();

	(if pa <= pb && pa <= pc {
		a
	} else if pb <= pa && pb <= pc {
		b
	} else {
		c
	} as u8)
}

#[cfg(feature = "simd")]
#[allow(clippy::many_single_char_names)]
pub fn encode_old(
	size: (usize, usize),
	pixel_size: usize,
	pixels: &mut [u8],
	old_pixels: &mut [u8],
	output: &mut [u8],
) -> usize {
	assert!(size.0 < u32::MAX as usize);
	assert!(size.1 < u32::MAX as usize);
	assert_eq!(pixel_size % 3, 0);
	assert!(pixel_size < u8::MAX as usize);
	assert_eq!(pixels.len(), size.0 * size.1 * pixel_size);

	assert_eq!(pixels.len(), old_pixels.len());

	for i in (size.0 * pixel_size..pixels.len()).rev() {
		// I think this is safe...
		let z = unsafe { old_pixels.get_unchecked_mut(i) };
		let a = *unsafe { pixels.get_unchecked(i - pixel_size) };
		let b = *unsafe { pixels.get_unchecked(i - size.0 * pixel_size) };
		let c = *unsafe { pixels.get_unchecked(i - (size.0 - 1) * pixel_size) };
		let x = unsafe { pixels.get_unchecked_mut(i) };

		let old_z = *z;
		*z = x.wrapping_sub(paeth(a, b, c));
		*x = z.wrapping_sub(old_z);
	}
	for i in (pixel_size..size.0 * pixel_size).rev() {
		let z = unsafe { old_pixels.get_unchecked_mut(i) };
		let a = *unsafe { pixels.get_unchecked(i - pixel_size) };
		let x = unsafe { pixels.get_unchecked_mut(i) };

		let old_z = *z;
		*z = x.wrapping_sub(a);
		*x = z.wrapping_sub(old_z);
	}
	if !pixels.is_empty() {
		for i in 0..pixel_size {
			let z = unsafe { old_pixels.get_unchecked_mut(i) };
			let x = unsafe { pixels.get_unchecked_mut(i) };

			let old_z = *z;
			*z = *x;
			*x = x.wrapping_sub(old_z);
		}
	}

	qoi::encode_to_buf(output, pixels, size.0 as u32, size.1 as u32).unwrap()
}

#[cfg(feature = "simd")]
#[allow(clippy::many_single_char_names)]
pub fn decode_old(
	size: (usize, usize),
	pixel_size: usize,
	pixels: &mut [u8],
	old_pixels: &mut [u8],
	data: &[u8],
) {
	assert!(size.0 < u32::MAX as usize);
	assert!(size.1 < u32::MAX as usize);
	assert_eq!(pixel_size % 3, 0);
	assert!(pixel_size < u8::MAX as usize);
	assert_eq!(pixels.len(), size.0 * size.1 * pixel_size);

	assert_eq!(pixels.len(), old_pixels.len());

	let header = qoi::decode_to_buf(pixels.as_mut(), data).unwrap();
	assert_eq!(header.width, size.0 as u32);
	assert_eq!(header.height, size.1 as u32);
	assert_eq!(header.channels.as_u8(), pixel_size as u8);

	if !pixels.is_empty() {
		for i in 0..pixel_size {
			let z = unsafe { old_pixels.get_unchecked_mut(i) };
			let x = unsafe { pixels.get_unchecked_mut(i) };

			*z = z.wrapping_add(*x);
			*x = *z;
		}
	}
	for i in pixel_size..size.0 * pixel_size {
		let z = unsafe { old_pixels.get_unchecked_mut(i) };
		let a = *unsafe { pixels.get_unchecked(i - pixel_size) };
		let x = unsafe { pixels.get_unchecked_mut(i) };

		*z = z.wrapping_add(*x);
		*x = z.wrapping_add(a);
	}
	for i in size.0 * pixel_size..pixels.len() {
		let z = unsafe { old_pixels.get_unchecked_mut(i) };
		let a = *unsafe { pixels.get_unchecked(i - pixel_size) };
		let b = *unsafe { pixels.get_unchecked(i - size.0 * pixel_size) };
		let c = *unsafe { pixels.get_unchecked(i - (size.0 - 1) * pixel_size) };
		let x = unsafe { pixels.get_unchecked_mut(i) };

		*z = z.wrapping_add(*x);
		*x = z.wrapping_add(paeth(a, b, c));
	}
}
