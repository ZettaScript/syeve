use crate::{protocol, util::*};

use sdl2::{self, event::Event, keyboard::Keycode, pixels::PixelFormatEnum, render::Texture};
use std::{
	net::{SocketAddr, UdpSocket},
	sync::Arc,
	thread::sleep,
	time::Duration,
};
use structopt::StructOpt;

const BUF: usize = 0x60_00_00;

#[derive(StructOpt, Debug)]
pub struct Opt {
	#[structopt(default_value = "0.0.0.0:8080")]
	address: String,
}

pub fn run(opt: Opt) {
	let (sender, recv) = crossbeam_channel::bounded::<(Vec<u8>, SocketAddr)>(30);
	let (rev_sender, rev_recv) =
		crossbeam_channel::bounded::<(protocol::ReverseRequest, SocketAddr)>(30);
	let socket = Arc::new(UdpSocket::bind(opt.address).expect("Cannot bind socket"));
	let socket_client = socket.clone();
	std::thread::spawn(move || run_server(socket, sender));
	std::thread::spawn(move || run_client(socket_client, rev_recv));
	run_window(recv, rev_sender);
}

fn run_server(socket: Arc<UdpSocket>, sender: crossbeam_channel::Sender<(Vec<u8>, SocketAddr)>) {
	let mut packet = vec![];
	let mut len = 0usize;

	loop {
		if 65_007usize
			.checked_sub(packet.len() - len)
			.map_or(true, |x| try_vallocplus(&mut packet, x).is_ok())
		{
			let plen = packet.len();
			if let Ok((nb, addr)) = socket.recv_from(&mut packet[len..plen]) {
				if nb != 0 {
					len += nb - 1;
					if len > BUF {
						println!("max buf");
						packet.clear();
						len = 0;
						continue;
					}
					//println!("subpacket {}", nb);
					if packet[len] == 0 {
						unsafe {
							packet.set_len(len);
						}
						packet.shrink_to_fit();
						//println!("end packet {}", len);
						sender.send((packet, addr)).unwrap();
						packet = vec![];
						len = 0;
					}
				}
			}
		}
	}
}

fn run_client(
	socket: Arc<UdpSocket>,
	recv: crossbeam_channel::Receiver<(protocol::ReverseRequest, SocketAddr)>,
) {
	for (req, addr) in recv.iter() {
		if let Err(e) = socket.send_to(&[req as u8], dbg!(addr)) {
			println!("Error sending reverse request: {:?}", e);
		} else {
			println!("Requested new sequence");
		}
	}
}

fn run_window(
	recv: crossbeam_channel::Receiver<(Vec<u8>, SocketAddr)>,
	sender: crossbeam_channel::Sender<(protocol::ReverseRequest, SocketAddr)>,
) {
	let mut decoder = syeve::Decoder::new();

	let sdl_context = sdl2::init().unwrap();
	let video_subsystem = sdl_context.video().unwrap();

	let window = video_subsystem
		.window("Conference server", 800, 600)
		.resizable()
		.position_centered()
		.build()
		.unwrap();

	let mut canvas = window.into_canvas().accelerated().build().unwrap();
	let texture_creator = canvas.texture_creator();

	let mut event_pump = sdl_context.event_pump().unwrap();
	let interval = Duration::from_millis(10);

	let mut texture: Option<(Texture, (u32, u32))> = None;

	'running: loop {
		for event in event_pump.poll_iter() {
			match event {
				Event::Quit { .. }
				| Event::KeyDown {
					keycode: Some(Keycode::Escape),
					..
				} => {
					break 'running;
				}
				_ => {}
			}
		}

		if let Ok((packet, addr)) = recv.try_recv() {
			match decoder.decode(&packet) {
				Ok(syeve::DecodedFrame {
					frame_number,
					pixels: frame,
					seq_number,
					size: frame_size,
					status,
					..
				}) => {
					println!(
						"Sequence {}\tFrame {}:\t{} bytes, {:?}",
						seq_number,
						frame_number,
						packet.len(),
						status
					);

					if status != syeve::DecodedFrameStatus::Ok {
						sender
							.try_send((protocol::ReverseRequest::NewSequence, addr))
							.ok();
					}

					let frame_size = (frame_size.0 as u32, frame_size.1 as u32);
					if let Some((texture, size)) = &mut texture {
						if size == &frame_size {
							texture
								.update(None, &frame, (frame_size.0 * 3) as usize)
								.unwrap();
							canvas.copy(texture, None, None).unwrap();
						} else {
							*texture = texture_creator
								.create_texture_streaming(
									PixelFormatEnum::RGB24,
									frame_size.0,
									frame_size.1,
								)
								.unwrap();
							*size = frame_size;
						}
					} else {
						texture = Some((
							{
								let mut texture = texture_creator
									.create_texture_streaming(
										PixelFormatEnum::RGB24,
										frame_size.0,
										frame_size.1,
									)
									.unwrap();
								texture
									.update(None, &frame, (frame_size.0 * 3u32) as usize)
									.unwrap();
								canvas.copy(&texture, None, None).unwrap();
								texture
							},
							frame_size,
						));
					}

					canvas.present();
				}
				Err(e) => {
					sender
						.try_send((protocol::ReverseRequest::NewSequence, addr))
						.ok();
					println!("Error decoding incoming frame: {:?}", e);
				}
			}
		}

		sleep(interval);
	}
}
