/// Allocate Vec without initializing
#[inline]
pub fn valloc<T>(n: usize) -> Vec<T> {
	let mut v = Vec::with_capacity(n);
	unsafe {
		v.set_len(n);
	}
	v
}

/// Extend Vec without initializing
#[inline]
pub fn try_vallocplus<T>(
	v: &mut Vec<T>,
	n: usize,
) -> Result<(), std::collections::TryReserveError> {
	v.try_reserve_exact(n)?;
	unsafe {
		v.set_len(v.len() + n);
	}
	Ok(())
}
