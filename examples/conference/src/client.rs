use crate::{protocol, util::*};

use rand::Rng;
use std::{net::UdpSocket, sync::Arc};
use structopt::StructOpt;

const BUF: usize = 0x01_00;

#[derive(StructOpt, Debug)]
pub struct Opt {
	address: String,
	#[structopt(default_value = "10", long)]
	fps: f64,
}

fn bgra_to_rgb(bgra: &[u8], rgb: &mut [u8]) {
	bgra.chunks_exact(4)
		.zip(rgb.chunks_exact_mut(3))
		.for_each(|(bgra, rgb)| {
			rgb[0] = bgra[2];
			rgb[1] = bgra[1];
			rgb[2] = bgra[0];
		});
}

pub async fn run(opt: Opt) {
	let mut capturer = scrap::Capturer::new(scrap::Display::primary().unwrap()).unwrap();

	let size = (capturer.width(), capturer.height());
	println!("Image size: {:?}", size);

	let mut encoder = syeve::Encoder::new(
		size,
		3,
		600,
	);
	encoder.set_seq_number(rand::thread_rng().gen());

	let (rev_sender, rev_recv) = crossbeam_channel::bounded::<protocol::ReverseRequest>(1);

	let socket = Arc::new(UdpSocket::bind("0.0.0.0:0").unwrap());
	socket.connect(opt.address).unwrap();

	let socket_server = socket.clone();
	std::thread::spawn(move || run_server(socket_server, rev_sender));

	let mut buf_frame = valloc(size.0 * size.1 * 3);
	let mut buf_enc = Vec::new();

	let mut interval =
		async_timer::Interval::platform_new(core::time::Duration::from_secs_f64(1.0 / opt.fps));

	loop {
		if let Ok(image) = capturer.frame() {
			bgra_to_rgb(&image, &mut buf_frame);

			if rev_recv.try_recv() == Ok(protocol::ReverseRequest::NewSequence) {
				encoder.new_seq();
			}

			encoder.encode(&mut buf_frame, &mut buf_enc).unwrap();

			println!(
				"Sequence {}\tFrame {}:\t{} bytes",
				encoder.get_seq_number(),
				encoder.get_frame_number(),
				buf_enc.len()
			);

			let iter = buf_enc.chunks_exact(65_006);
			let rem = iter.remainder();
			for chunk in iter {
				if let Err(e) = socket.send(&[chunk, &[1u8]].concat()) {
					println!("Error sending packet: {:?}", e);
				}
				std::thread::sleep_ms(10);
			}
			if !rem.is_empty() {
				if let Err(e) = socket.send(&[rem, &[0u8]].concat()) {
					println!("Error sending packet: {:?}", e);
				}
			}
			buf_enc.clear();
		}
		interval.as_mut().await;
	}
}

fn run_server(socket: Arc<UdpSocket>, sender: crossbeam_channel::Sender<protocol::ReverseRequest>) {
	let mut packet = vec![];
	let mut len = 0usize;

	loop {
		if 65_007usize
			.checked_sub(packet.len() - len)
			.map_or(true, |x| try_vallocplus(&mut packet, x).is_ok())
		{
			let plen = packet.len();
			if let Ok(nb) = socket.recv(&mut packet[len..plen]) {
				if nb != 0 {
					len += nb - 1;
					if len > BUF {
						println!("max buf");
						packet.clear();
						len = 0;
						continue;
					}
					//println!("subpacket {}", nb);
					if packet[len] == 0 {
						unsafe {
							packet.set_len(len);
						}
						packet.shrink_to_fit();
						//println!("end packet {}", len);
						if packet[0] == 1 {
							sender.send(protocol::ReverseRequest::NewSequence).unwrap();
						}
						packet = vec![];
						len = 0;
					}
				}
			}
		}
	}
}
