mod client;
mod play;
mod protocol;
mod rec;
mod server;
mod util;

use structopt::StructOpt;

#[derive(Debug, StructOpt)]
enum Mode {
	#[structopt(name = "client")]
	Client(client::Opt),
	#[structopt(name = "play")]
	Play(play::Opt),
	#[structopt(name = "rec")]
	Rec(rec::Opt),
	#[structopt(name = "server")]
	Server(server::Opt),
}

#[derive(Debug, StructOpt)]
#[structopt(name = "conference")]
struct Opt {
	#[structopt(subcommand)]
	mode: Mode,
}

fn main() {
	let opt = Opt::from_args();
	match opt.mode {
		Mode::Client(opt) => futures::executor::block_on(client::run(opt)),
		Mode::Play(opt) => futures::executor::block_on(play::run(opt)),
		Mode::Rec(opt) => futures::executor::block_on(rec::run(opt)),
		Mode::Server(opt) => server::run(opt),
	}
}
