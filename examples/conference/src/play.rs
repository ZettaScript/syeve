use crate::util::*;

use sdl2::{self, event::Event, keyboard::Keycode, pixels::PixelFormatEnum, render::Texture};
use std::{io::Read, thread::sleep, time::Duration};
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
pub struct Opt {
	#[structopt(default_value = "10", long)]
	fps: f64,
	#[structopt(default_value = "video.syeve")]
	file: String,
}

pub async fn run(opt: Opt) {
	let (sender, recv) = crossbeam_channel::bounded(30);
	std::thread::spawn(move || run_window(recv));
	reader(opt, sender).await;
}

pub async fn reader(opt: Opt, sender: crossbeam_channel::Sender<Vec<u8>>) {
	let mut file = std::fs::File::open(opt.file).expect("Could not open file");

	let mut interval =
		async_timer::Interval::platform_new(core::time::Duration::from_secs_f64(1.0 / opt.fps));

	let mut size_buf = [0u8; 4];
	while file.read_exact(&mut size_buf).is_ok() {
		let size = u32::from_be_bytes(size_buf) as usize;
		assert!(size < 0x60_00_00);
		let mut packet = valloc(size);
		file.read_exact(&mut packet).unwrap();
		sender.send(packet).unwrap();
		interval.as_mut().await;
	}
}

fn run_window(recv: crossbeam_channel::Receiver<Vec<u8>>) {
	let mut decoder = syeve::Decoder::new();

	let sdl_context = sdl2::init().unwrap();
	let video_subsystem = sdl_context.video().unwrap();

	let window = video_subsystem
		.window("Syeve file player", 800, 600)
		.resizable()
		.position_centered()
		.build()
		.unwrap();

	let mut canvas = window.into_canvas().accelerated().build().unwrap();
	let texture_creator = canvas.texture_creator();

	let mut event_pump = sdl_context.event_pump().unwrap();
	let interval = Duration::from_millis(10);

	let mut texture: Option<(Texture, (u32, u32))> = None;

	'running: loop {
		for event in event_pump.poll_iter() {
			match event {
				Event::Quit { .. }
				| Event::KeyDown {
					keycode: Some(Keycode::Escape),
					..
				} => {
					break 'running;
				}
				_ => {}
			}
		}

		if let Ok(packet) = recv.try_recv() {
			match decoder.decode(&packet) {
				Ok(syeve::DecodedFrame {
					frame_number,
					pixels: frame,
					seq_number,
					size: frame_size,
					status,
					..
				}) => {
					println!(
						"Sequence {}\tFrame {}:\t{} bytes, {:?}",
						seq_number,
						frame_number,
						packet.len(),
						status
					);

					let frame_size = (frame_size.0 as u32, frame_size.1 as u32);
					if let Some((texture, size)) = &mut texture {
						if size == &frame_size {
							texture
								.update(None, &frame, (frame_size.0 * 3) as usize)
								.unwrap();
							canvas.copy(texture, None, None).unwrap();
						} else {
							*texture = texture_creator
								.create_texture_streaming(
									PixelFormatEnum::RGB24,
									frame_size.0,
									frame_size.1,
								)
								.unwrap();
							*size = frame_size;
						}
					} else {
						texture = Some((
							{
								let mut texture = texture_creator
									.create_texture_streaming(
										PixelFormatEnum::RGB24,
										frame_size.0,
										frame_size.1,
									)
									.unwrap();
								texture
									.update(None, &frame, (frame_size.0 * 3u32) as usize)
									.unwrap();
								canvas.copy(&texture, None, None).unwrap();
								texture
							},
							frame_size,
						));
					}

					canvas.present();
				}
				Err(e) => println!("Error decoding incoming frame: {:?}", e),
			}
		}

		sleep(interval);
	}
}
