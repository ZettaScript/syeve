fn main() {
	let niters: usize = 100;

	let path = std::env::args().nth(1).expect("Need <path>");
	let decoder = png::Decoder::new(std::fs::File::open(path).unwrap());
	let mut reader = decoder.read_info().unwrap();
	let mut pixels = vec![0; reader.output_buffer_size()];
	let info = reader.next_frame(&mut pixels).unwrap();
	pixels.truncate(info.buffer_size());
	let pixels_save = pixels.clone();

	let size = (info.width, info.height);
	let mut data = Vec::new();
	let mut encoder = syeve::Encoder::new(
		(size.0 as usize, size.1 as usize),
		reader.info().bytes_per_pixel(),
		30,
	);
	encoder.encode(&mut pixels, &mut data).unwrap();
	println!("Output 1 size: {}", data.len());
	data.clear();
	pixels.copy_from_slice(&pixels_save);
	encoder.encode(&mut pixels, &mut data).unwrap();
	println!("Output 2 size: {}", data.len());

	let start = std::time::Instant::now();
	for _ in 0..niters {
		pixels.copy_from_slice(&pixels_save);
		encoder.encode(&mut pixels, std::io::sink()).unwrap();
	}
	let dur_own = std::time::Instant::now().duration_since(start);

	let start = std::time::Instant::now();
	for _ in 0..niters {
		let mut encoder = png::Encoder::new(std::io::sink(), size.0, size.1);
		encoder.set_color(info.color_type);
		encoder.set_depth(info.bit_depth);
		let mut writer = encoder.write_header().unwrap();
		writer.write_image_data(&pixels_save).unwrap();
	}
	let dur_png = std::time::Instant::now().duration_since(start);

	println!("Own: {} ms", dur_own.as_millis());
	println!("PNG: {} ms", dur_png.as_millis());
}
